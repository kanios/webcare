<?php

namespace App\Controller\Admin;

use App\Entity\Remark;
use App\Form\RemarkType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RemarkController
 * @package App\Controller
 */
class RemarkController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $remarkRepo = $em->getRepository('App:Remark');

        $remarks = $remarkRepo->findAll();

        return $this->render('admin/remark/index.html.twig', ['remarks' => $remarks] );
    }

    public function newAction(Request $request)
    {
        $remark = new Remark();

        $form = $this->createForm(RemarkType::class, $remark);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this
                ->getDoctrine()
                ->getManager()
            ;

            $remark
                ->setDate(new \DateTime())
                ->setUserId(2)
            ;

            if (empty($remark->getText()))
                throw new Exception("Error: Pusty komentarz!");

            $em->persist($remark);
            $em->flush();

            return $this->redirectToRoute('admin_remark_index');
        }

        return $this->render('admin/remark/new.html.twig', ['form' => $form->createView()]);
    }
}
